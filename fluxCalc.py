
from __future__ import print_function
import argparse
import numpy as np

class GetFlux():

    def __init__(self, sig, bw, tsys, gain, tobs, npol, width, period):

        self.sig = sig    # Required signal-to-noise ratio
        self.bandwidth = bw     # Bandwidth (in Hz)
        self.tsys = tsys   # System temperature (K)
        self.gain = gain   # Telescope gain (K/Jy)
        self.tobs = tobs   # Integration time (s)
        self.npol = npol   # Number of polarisations
        self.width = width  # Pulse width (turns)
        self.period = period # Pulse period (s)
        self.width_s = None

    def flux(self):


        self.width_s = self.width * self.period

        term1_num = self.sig * self.tsys
        term1_denom = self.gain * np.sqrt(self.bandwidth * self.tobs * self.npol)
        term2 = np.sqrt(self.width_s / (self.period - self.width_s))

        result = (term1_num / term1_denom) * term2

        return result

def main():

    # Process command line args
    parser = argparse.ArgumentParser(description='Gaussian pulse profile generator')
    parser.add_argument('-s','--sig', help='Required signal-to-noise ratio',
                        required=True, type=float)
    parser.add_argument('-w','--width', help='Pulse width (turns)',
                        required=True, type=float)
    parser.add_argument('-p','--period', help='Pulse period (secs)',
                        required=True, type=float)
    parser.add_argument('-b','--bandwidth', help='Bandwidth (Hz)',
                        required=False, type=float, default=320e6)
    parser.add_argument('-t','--tsys', help='System temperature (K)',
                        required=False, type=float, default=25.0)
    parser.add_argument('-g','--gain', help='Telescope gain (K/Jy)',
                        required=False, type=float, default=1.0)
    parser.add_argument('-i','--tobs', help='Integration time',
                        required=False, type=float, default=600.0)
    parser.add_argument('-n','--npol', help='Number of polarisations',
                        required=False, type=int, default=1)

    args = parser.parse_args()

    get_flux = GetFlux(args.sig, args.bandwidth, args.tsys,
                       args.gain, args.tobs, args.npol,
                       args.width, args.period)
    this_flux = get_flux.flux()
    print(this_flux)

if __name__ == '__main__':
    main()
