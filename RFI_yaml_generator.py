import argparse
import os
import random as rd
import string

import yaml
from numpy import random

# pylint: disable=C0301,R0914


def main():
    """Module providing a function to Generate YAML file containing various kinds of 
    RFI chunks which can be injected into filterbanks"""
    parser = argparse.ArgumentParser(description="RFI Configuration file Generator\n(Seperate each chunk using a comma & use 'random' or 'r' to generate a random number for that parameter)")
    group = parser.add_argument_group("Configuration mode of Generator")
    # group.add_argument("-B","--batch",help="Name of file that configures a batch of Test-vectors",required=False)
    group.add_argument("-o","--output",help="Name of the output file",type=str)
    group.add_argument("--force",help="Overwrite existing file (if exists)", action='store_true')

    group=parser.add_argument_group("Test vector Configuration")
    group.add_argument("-t","--tobs",help="Duration of Observation (s)",type=float,default=10.0)
    group.add_argument("-n","--nchans",help="Number of Frequency Channels",type=int,default=4096)
    group.add_argument("-F","--fch1",help="First Frequency Channel (MHz)",type=float,default=1670)
    group.add_argument("-f","--foff",help="Width of Frequency Channel (MHz)",type=float,default=-0.078125)

    group = parser.add_argument_group("Narrowband & Periodic RFIs")
    group.add_argument('-N','--narrowband',help="Set Narrowband RFI ON",action='store_true')
    group.add_argument('-x','--nchunks',help='Number of narrowband RFI Chunks (Completely Random Process)',type=int)
    group.add_argument('-a','--ntstart',help="Start time of Narrowband (Optional)",type=str)
    group.add_argument('-b','--ntstop',help="Stop time of Narrowband (Optional)",type=str)
    group.add_argument('-c','--nduration',help="Duration of Narrowband (Optional)",type=str)
    group.add_argument('-d','--nfbegin',help="Start Frequency of Narrowband (Optional)",type=str)
    group.add_argument('-e','--nfend',help="Stop Frequency of Narrowband (Optional)",type=str)
    group.add_argument('-P','--periodic',help="Set Periodicity on", action='store_true')
    group.add_argument('-p','--period',help="Period of Periodic RFI",type=str)
    group.add_argument('-D','--duty',help="Duty cycle of Periodic RFI",type=str)
    group.add_argument('-z','--nmag',help='Magnitude of the Narrowband RFI',type=str)

    group = parser.add_argument_group("Broadband RFIs")
    group.add_argument('-W','--broadband',help="Set Broadband RFI ON",action='store_true')
    group.add_argument('-X','--bchunks',help='Number of broadband RFI Chunks (Completely Random Process)',type=int)
    group.add_argument('-k','--btstart',help="Start time of Broadband (Optional)",type=str)
    group.add_argument('-l','--btstop',help="Stop time of Boardband (Optional)",type=str)
    group.add_argument('-m','--bduration',help="Duration of Broadband (Optional)",type=str)
    group.add_argument('-Z','--bmag',help='Magnitude of the Narrowband RFI',type=str)

    args = parser.parse_args()

    def rfi_settings():
        """
        Sets the filename properties
        based on the RFI settings
        """
        default_settings = {
            'nbid' : 'nb0',
            'pid' : 'p0',
            'bbid' : 'bb0',
        }
        if args.narrowband:
            default_settings['nbid'] = 'nb1'
        if args.periodic:
            default_settings['pid'] = 'p1'
        if args.broadband:
            default_settings['bbid'] = 'bb1'

        settings = tuple(default_settings.values())
        return f"{settings[0]}_{settings[1]}_{settings[2]}"

    # Do we have a directory for the RFI configs?
    output_dir = "rfi_configs"
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)

    if args.output:
        export_path = os.path.join(output_dir, args.output)
        if os.path.isfile(export_path):
            if not args.force:
                raise OSError("Cannot write to {}. File exists".format(export_path))
            else:
                print("Overwriting {}".format(export_path))
                os.remove(export_path)
        print("Will write to {}".format(export_path))
    else:
        # Use args to populate filename
        rfi_args = rfi_settings()
        # Get random 4-digit alphanumeric string to identify file.
        file_id = ''.join(rd.choices(string.ascii_letters + string.digits, k=4))
        # For formal PSS testing we define the RFI config file as follows
        filename = f"rficonfig_{file_id}_{args.tobs}_{args.nchans}_{args.fch1}_{args.foff}_{rfi_args}.yaml"
        # Where rfi_args specifies the type of RFI that are enabled. See rfi_settings().
        export_path = os.path.join(output_dir, filename)

    # We get every parameter as a string as we even want to give an option for randomly generated number and then convert them into float or generate a suitable random number
    n_tstart, n_tstop, n_period, n_duty, n_fstart, n_fstop, n_mag = None, None, None, None, None, None, None
    b_tstart, b_tstop, b_mag = None, None, None
    data = []

    # Checking if Narrowband Chunks have to be generated
    if args.narrowband or args.periodic:
        print('\nCreating Narrowband Chunks..')
        if args.nchunks is not None:
            n_tstart = [float(random.randint(0,args.tobs*100-10)/100) for i in range(args.nchunks)]
            n_tstop = [float(random.randint(n_tstart[i]*100+1,args.tobs*100)/100) for i in range(len(n_tstart))]
 
        else:
            n_tstart = args.ntstart.split(',')
            n_tstart = [float(random.randint(0,args.tobs*100-10)/100) if l in ['random','r'] else float(l) for l in n_tstart]

            if args.ntstop is not None:
                n_tstop = args.ntstop.split(',')
                n_tstop = [float(random.randint(n_tstart[l]*100+1,args.tobs*100)/100) if n_tstop[l] in ['random','r'] else float(n_tstop[l]) for l in range(len(n_tstop))]
            else:
                if args.nduration is None:
                    n_tstop = [float(random.randint(n_tstart[i]*100+1,args.tobs*100)/100) for i in range(len(n_tstart))]
                else:
                    n_tstop = [0 for i in n_tstart]
                    n_duration = args.nduration.split(',')
                    for i in range(len(n_duration)):
                        if n_duration[i] in ['random','r']:
                            n_tstop[i] = float(random.randint(n_tstart[i]*100+1,args.tobs*100)/100)
                        else:
                            n_tstop[i] = n_tstart[i]+float(n_duration[i])

        n_period,n_duty = [0 for i in n_tstart],[0 for i in n_tstart]

        # Checks if there are any periodicities included
        if args.periodic:
            print('Generating Periods and Duty-cycle for RFI chunks..')
            if args.period is not None:
                period = args.period.split(',')
                n_period = [float(random.randint((n_tstop[i]-n_tstart[i])*10000)/10000) if period[i] in ['random','r'] else float(period[i]) for i in range(len(n_tstart))]
            else:
                n_period = [float(random.randint((n_tstop[i]-n_tstart[i])*10000)/10000) for i in range(len(n_tstart))]
        if args.periodic:
            if args.duty is not None:
                duty = args.duty.split(',')
                n_duty = [float(random.randint(1000)/1000) if duty[i] in ['random','r'] else float(duty[i]) for i in range(len(n_tstart))]
            else:
                n_duty = [float(random.randint(1000)/1000) for i in range(len(n_tstart))]

        if args.nfbegin:
            n_fstart = args.nfbegin.split(',')
            n_fstart = [int(random.randint(args.fch1+args.foff*args.nchans,args.fch1)) if l in ['random','r'] else int(l) for l in n_fstart]
        else:
            n_fstart = [int(random.randint(args.fch1+args.foff*args.nchans,args.fch1)) for i in n_tstart]

        if args.nfend:
            n_fstop = args.nfend.split(',')
            n_fstop = [int(random.randint(n_fstart[l],args.fch1)) if n_tstop[l] in ['random','r'] else int(n_fstop[l]) for l in range(len(n_fstart))]
        else:
            n_fstop = [int(random.randint(n_fstart[i],args.fch1)) for i in range(len(n_fstart))]

        if args.nmag:
            n_mag = args.nmag.split(',')
            n_mag = [random.uniform(1.0,6.0) if n_mag[l] in ['random','r'] else int(n_mag[l]) for l in range(len(n_mag))]
        else:
            n_mag = [random.uniform(1.0,6.0) for i in range(len(n_fstart))]


        # Arranging the parameter in form of a dictionary
        for i in range(len(n_tstart)):
            data.append({'fstart':n_fstart[i],'fstop':n_fstop[i],'tstart':n_tstart[i],'tstop':n_tstop[i],'period':n_period[i],'duty':n_duty[i],'mag':n_mag[i]})

    # Checking if Broadband chunks have to be created
    if args.broadband:
        print('\nCreating Broadband Chunks..')
        if args.bchunks is not None:
            b_tstart = [float(random.randint(0,args.tobs*100-10)/100) for i in range(args.bchunks)]
            b_tstop = [float(random.randint(b_tstart[i]*100+1,args.tobs*100)/100) for i in range(len(b_tstart))]

        else:
            b_tstart = args.btstart.split(',')
            b_tstart = [float(random.randint(0,args.tobs*100-10)/100) if l in ['random','r'] else float(l) for l in b_tstart]

            if args.btstop is not None:
                b_tstop = args.btstop.split(',')
                b_tstop = [float(random.randint(b_tstart[l]*100+1,args.tobs*100)/100) if b_tstop[l] in ['random','r'] else float(b_tstop[l]) for l in range(len(b_tstop))]

            if args.btstop is None:
                if args.bduration is None:
                    b_tstop = [float(random.randint(b_tstart[i]*100+1,args.tobs*100)/100) for i in range(len(b_tstart))]
                else:
                    b_tstop = [0.0 for i in range(len(b_tstart))]
                    b_duration = args.bduration.split(',')
                    for i in range(len(b_duration)):
                        if b_duration[i] in ['random','r']:
                            b_tstop[i] = float(random.randint(b_tstart[i]*100+1,args.tobs*100)/100)
                        else:
                            b_tstop[i] = b_tstart[i]+float(b_duration[i])
        if args.bmag:
            b_mag = args.bmag.split(',')
            b_mag = [random.uniform(1.0,6.0) if b_mag[l]==['random','r'] else int(b_mag[l]) for l in range(len(b_mag))]
        else:
            b_mag = [random.uniform(1.0,6.0) for i in range(len(b_tstart))]

        # Arranging them inside a dictionary with frequency range spanning entire bandwidth of observation
        for i in range(len(b_tstart)):
            data.append({'fstart':args.fch1+args.foff*args.nchans,'fstop':args.fch1,'tstart':b_tstart[i],'tstop':b_tstop[i],'period':0,'mag':b_mag[i]})

    print('Creation of chunks - Done..')

    with open(export_path,'a') as rfi_file:
        yaml.dump(data,rfi_file)


if __name__=='__main__':
    main()
