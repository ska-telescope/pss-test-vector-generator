"""
    **************************************************************************
    |                                                                        |
    |                      Gaussian profile generator                        |
    |                                                                        |
    **************************************************************************
    | Description:                                                           |
    |                                                                        |
    | Generates an 1-D ascii file containing a gaussian profile for          |
    | injection into a noise filterbank. Files produced with have the naming |
    | convention:  Gaussian_DC=<duty cycle>_BINS=<nbins>_FWHP=<FWHP>.asc     |
    | e.g.,                                                                  |
    |  Gaussian_DC=0.01_BINS=128_FWHM_1.28.asc                               |
    |                                                                        |
    **************************************************************************
    | Author: Benjamin Shaw                                                  |
    | Email : benjamin.shaw@manchester.ac.uk                                 |
    **************************************************************************
    | Gaussian profile generation command Line Arguments:                    |
    |                                                                        |
    | Required:                                                              |
    | =========                                                              |
    |                                                                        |
    | --nbins (int)         The number of bins across the pulse profile      |
    |                                                                        |
    | --dc (float)          The duty cycle of the pulse.                     |
    |                       (0 < dc < 1)                                     |
    | Optional:                                                              |
    | =========                                                              |
    |                                                                        |
    | --show                Display profile to user before writing to file   |
    |                                                                        |
    | --norm                Normalise the profile to the peak (i.e., the max |
    |                       power value is 1)                                |
    |                                                                        |
    | --dir (string)        The directory in which to write the profile file.|
    |                       (If not selected, profiles will be written to .) |
    **************************************************************************
    | License:                                                               |
    |                                                                        |
    | Copyright 2021 University of Manchester                                |
    |                                                                        |
    |Redistribution and use in source and binary forms, with or without      |
    |modification, are permitted provided that the following conditions are  |
    |met:                                                                    |
    |                                                                        |
    |1. Redistributions of source code must retain the above copyright       |
    |notice,                                                                 |
    |this list of conditions and the following disclaimer.                   |
    |                                                                        |
    |2. Redistributions in binary form must reproduce the above copyright    |
    |notice, this list of conditions and the following disclaimer in the     |
    |documentation and/or other materials provided with the distribution.    |
    |                                                                        |
    |3. Neither the name of the copyright holder nor the names of its        |
    |contributors may be used to endorse or promote products derived from    |
    |this                                                                    |
    |software without specific prior written permission.                     |
    |                                                                        |
    |THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS     |
    |"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT       |
    |LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A |
    |PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT      |
    |HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,  |
    |SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT        |
    |LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,   |
    |DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON       |
    |ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR      |
    |TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE  |
    |USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH        |
    |DAMAGE.                                                                 |
    **************************************************************************
"""
# Import modules
from __future__ import print_function
import sys
import os
import argparse
import logging
import scipy.stats as stats
import numpy as np
import matplotlib.pyplot as plt
from lmfit.models import GaussianModel

logging.basicConfig(format='1|%(asctime)s|%(levelname)s|%(funcName)s|%(filename)s#%(lineno)d|%(message)s',
                    datefmt='%Y-%m-%dT%I:%M:%S',
                    level=logging.INFO)

class MakeGaussianProfile():

    def __init__(self, nbins, dc, loc=False, show=False, norm=False):

        self.nbins = nbins   # Number of bins across profile
        self.duty = dc       # Duty cycle
        self.show = show     # Display plots
        self.norm = norm     # Normalise to peak
        self.sigma = None    # Standard deviation of pulse
        self.power = None    # Power series
        self.fw_bins = None  # FWHP (in bins)
        self.bins = None     # Bins
        self.loc = loc

    def check_inputs(self):
        """
        Checks inputs makes sense
        True if yes, else false. 
        """
        outcome = True
        if self.check_dc_val(self.duty):
            outcome = True
        else:
            logging.error("======FAILURE=======")
            logging.error("Invalid value for -d. Duty cycle must strictly be >0 and <1")
            outcome = False
            return outcome

        return outcome

    @staticmethod
    def check_dc_val(duty):
        """
        Checks duty cycle makes sense. 
        True, if yes, else false
        """
        if duty >= 1.0:
            return False
        if duty == 0.0:
            return False
        return True

    @staticmethod
    def dc_to_fwhp_bins(nbins, duty):
        """
        Converts duty cycle to a
        full width at half power (FWHP)
        """
        fwhp_bins = nbins * duty
        return fwhp_bins

    @staticmethod
    def fwhp_to_sigma(fw_bins):
        """
        Converts a full width at half power
        to a standard deviation
        """
        sigma = fw_bins / (2.0 * np.sqrt(2*np.log(2.0)))
        return sigma

    def plot(self):
        """
        Plots generated profile to console
        """
        plt.plot(self.bins, self.power, 'k-')
        plt.xlabel("Bin", fontsize=15)
        plt.ylabel("Probability Density", fontsize=15)
        plt.title("FWHP: {}, Sigma: {}".format(self.fw_bins, self.sigma))
        plt.axhline(np.max(self.power) / 2.0)
        plt.grid()
        plt.tight_layout()
        plt.show()

    def write_out(self, filename):
        """
        Writes out generated profile as ascii file
        """
        if os.path.isfile(filename):
            logging.error("======FAILURE=======")
            raise Exception("Profile already exists - cannot proceed")

        logging.info("Writing file {}".format(filename))
        for i in range(0, len(self.power)):
            with open(filename, 'a') as fout:
                fout.write(str(self.power[i]) + "\n")
        fout.close()

    @staticmethod
    def do_least_sq(power, xvals):
        """
        Fits a gaussian to the demanded profile and checks
        values against those demanded. 
        """
        model = GaussianModel()
        params = model.guess(power, x=xvals)
        result = model.fit(power, params, x=xvals)
        return result

    def filecheck(self, data):
        """
        Checks the contents of the data
        generated make sense.
        """
        outcome = True
        if len(data) != self.nbins:
            outcome = False
        return outcome

    def generate_profile(self):
        """
        Main method and entrypoint
        """
        # Update class variables
        if self.check_inputs():
            logging.info("Inputs checked - can proceed")

            # Define sampling
            self.bins = np.linspace(0, self.nbins - 1, self.nbins)

            # Define centre position of Gaussian
            centre = self.nbins / 2.0

            # Calculate the number of bins spanning the half-power
            self.fw_bins = self.dc_to_fwhp_bins(self.nbins, self.duty)

            # Calculate sigma
            self.sigma = self.fwhp_to_sigma(self.fw_bins)

            # Generate Gaussian values
            self.power = stats.norm.pdf(self.bins, centre, self.sigma)

            # Normalise if requested
            if self.norm:
                self.power = self.power / np.max(self.power)

            # Show diagnostic plot if requested
            if self.show:
                logging.info("Program will continue when plot is closed....")
                self.plot()

            # Define output filename and do write
            filename = "Gaussian_DC={}_BINS={}_FWHM_{}.asc".format(self.duty, self.nbins, self.fw_bins)
            if self.loc:
                 filename = str(self.loc) + "/" + filename
            self.write_out(filename)

            # Do a least-squares fit on the generated data and
            # check the parameters make sense
            result = self.do_least_sq(self.power, self.bins)

            # Print comparison between demanded and computed values
            logging.info("======Results=======")
            logging.info("Demanded duty cycle: {}".format(self.duty))
            logging.info("Demanded FWHP: {} bins. Actual FWHP: {} bins".format(self.fw_bins, result.params['fwhm'].value))
            logging.info("Demanded stdev: {}. Actual stdev: {}".format(self.sigma, result.params['sigma'].value))
            logging.info("Demanded mean: {}. Actual mean: {}".format(centre, result.params['center'].value))
            if self.norm:
                logging.info("Demanded height: 1.0. Actual height: {}".format(result.params['height'].value))
            else:
                logging.info("Height: {}".format(result.params['height'].value))

            # Check file contains what it should
            final_power = np.loadtxt(filename, unpack=True)
            if not self.filecheck(final_power):
                logging.warning("Generated file doesn't look right...")
            else:
                logging.info("File generation complete")
        return self.power

def main():

    # Process command line args
    parser = argparse.ArgumentParser(description='Pulse profile generator')
    parser.add_argument('-n', '--nbins', help='Number of bins across profile',
                        required=True, type=int)
    parser.add_argument('-d', '--dc', help='Duty cycle of pulse (as fraction)',
                        required=True, type=float)
    parser.add_argument('-s', '--show', help='Show diagnostic plots',
                        action='store_true')
    parser.add_argument('-p', '--norm', help='Normalise to peak',
                        action='store_true')
    parser.add_argument('-D', '--dir', help='Location of profiles',
                        required=False, type=str)
    args = parser.parse_args()

    profile = MakeGaussianProfile(args.nbins, args.dc, args.dir, args.show, args.norm)
    profile.generate_profile()


if __name__ == '__main__':
    print(__doc__)
    main()
