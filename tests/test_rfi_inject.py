#!/usr/bin/env python

"""
*****************************************************
|       Testing RFI Injected Filterbank             |
*****************************************************

Description: Verifies if a filterbank file contains RFI or not using several method

Method 1: Kolmogorov-Smirnov Test (KS-Test), where the underlying distribution of the filterbank is compared to Gaussian Noise with chosen Mean and Std
Method 2: Expected Mean is calculated using the RFI configuration (.yaml) file and compared with the actual mean of the Filterbank. The test is passed if 
            the difference of means is not more than 5% of the mean. 
            Note: User can provide the YAML file in the arguments or the location of the folder of rfi_config files, where the program searches for the 
            YAML file using the RFI_ID from filename of the test-vector.
"""


import argparse
import os

import numpy as np
import yaml
from scipy.stats import kstest
from sigpyproc.readers import FilReader

# pylint: disable=W1202
# pylint: disable=C0301
# pylint: disable=W0611
# pylint: disable=R0914

def fexists(this_file):
    """
    Checks if a file exists. True if yes, else False
    """
    if os.path.isfile(this_file):
        print("File {} found".format(this_file))
        return True
    else:
        print("File {} not found".format(this_file))
        return False

def extract(vector):
    """
    Takes a test vector filename and extracts
    the random number seed and the gitlab hash,
    returning the remaining parameters as a list.
    """
    fields = vector.split("_")[:-1]
    fields.pop(1)
    return fields

def load_yaml(yamlfile):
    """
    Loads in the YAML file. Checks it's valid YAML.
    Exception raised if not.
    """
    with open(yamlfile, 'r') as this_yaml:
        try:
            pars = yaml.load(this_yaml, Loader=yaml.FullLoader)
        except yaml.parser.ParserError:
            print("{} not valid YAML".format(yamlfile))
            raise Exception("YamlError")
    return pars

def ks_test(input_filbank,mean,std):
    """
    Function to perform K-S Test using a numpy method.
    It checks if the underlying distribution of the filterbank matches with that of a gaussian noise
    """
    print ('Perfoming Kolmogorov-Smirnov Test')
    fil = FilReader(input_filbank)
    fil_block = fil.read_block(0,fil.header.nsamples).reshape(fil.header.nchans*fil.header.nsamples)

    res = kstest(fil_block,np.random.normal(mean,std,(1024)))
    print('Statistic =',res.statistic)
    print ('P-Value =',res.pvalue)
    if res.pvalue<0.05:
        print ('KS-test failed. Evidence of RFI found in test vector')
    else:
        print('No evidence of RFI within a 95% confidence level')

def expect_mean(input_filbank,rfi_yaml_file,mean,std):
    """
    This function tries to predict the mean of RFI Injected filterbank and compare with the actual mean of it.
    It uses the RFI YAML Configuration file used to generate it and compare the both the mean and passes the test if the threshold is under 5%
    """
    print('Reading Filterbank file')
    fil = FilReader(input_filbank)
    tsamp = fil.header.tsamp
    nchans = fil.header.nchans
    fch1 = fil.header.fch1
    foff = fil.header.foff
    total_samples = nchans*fil.header.nsamples

    print('Reading RFI YAML Configuration File')
    rfi = load_yaml(rfi_yaml_file)

    print ('Calculating the Expected Mean')
    for i in rfi:
        tstart = int(np.floor(i['tstart']/tsamp))
        tstop = int(np.floor(i['tstop']/tsamp))
        fstart = int((fch1-i['fstart']+foff*nchans)/foff)
        fstop = int((fch1-i['fstop']+foff*nchans)/foff)
        period = int(np.floor(i['period']/tsamp))
        mag = i['mag']

        if period==0:
            mean+= (tstop-tstart)*(fstop-fstart)*mag*std/total_samples
        else:
            on_time = period*i['duty']*int((tstop-tstart)/period)
            mean+= on_time*(fstop-fstart)*mag*std/total_samples

    print('Expected Mean =',mean)
    observed_mean = np.mean(fil.read_block(0,fil.header.nsamples))
    print('Observed Mean =',observed_mean)

    if np.abs((observed_mean-mean)/mean) < 0.05:
        print('The means match (+/- 5% threshold)\nTEST PASSED!')
    else:
        if observed_mean>mean:
            print('Observed Mean is greater than expected mean!')
        else:
            print('Expected Mean is greater than Observed mean!')

def main():
    """
    Main method
    """
    parser = argparse.ArgumentParser(description="Testing if the RFI Injection is successful. If using KS-Test, set ks_test flag and RFI_file arguments NOT required")
    parser.add_argument('filbank', help='Path to the test-vector Generator',type=str)
    parser.add_argument('--rfi','-r',help='Path the RFI YAML Configuration file (if using expect-mean() and have a non-conventional YAML file)',type=str)
    parser.add_argument('--mean','-u',help='Mean of the Noise File (Filterbank) [default = 95.5]',type=float,default=95.5)
    parser.add_argument('--std','-s',help='Standard Deviation of the Noise File (Filterbank) [default = 24]',type=float,default=24)
    parser.add_argument('--rfi_loc','-l',help='Location of the directory of the RFI Config files (if using expect-mean() and search for the key-ID)',type=str)
    parser.add_argument('--ks_test','-k',help='Set the flag if you want to use the K-S Test [No RFI_file Optional Arguments required]',action='store_true',required=False)
    args = parser.parse_args()

    rfi_yaml_file = None
    if args.rfi:
        args.rfi_loc=None           #Setting the RFI_loc to None in order to avoid searching for the file again
        if not fexists(args.rfi):
            exit(1)
        rfi_yaml_file = args.rfi
    if args.rfi_loc:
        files = os.listdir(args.rfi_loc)
        for i in files:
            if extract(args.filbank)[-1] in i:
                rfi_yaml_file = os.path.join(args.rfi_loc,i)

    #Double chekcing the existence of RFI YAML File
    print ("Filterbank File: {}".format(args.filbank))
    if not args.ks_test:
        if not fexists(rfi_yaml_file):
            exit(1)
            

    if args.ks_test:
        ks_test(input_filbank=args.filbank,mean=args.mean,std=args.std)
    else:
        expect_mean(input_filbank=args.filbank,rfi_yaml_file=rfi_yaml_file,
                    mean=args.mean,std=args.std)

if __name__=='__main__':
    main()
