#!/bin/bash

threshold=6.0
mkdir score

# Lint GenerateGaussian.py
python -m pylint GenerateGaussian.py | tee score/result.txt
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' score/result.txt)
echo "Pylint score was $score"
if (( $(echo "$threshold > $score" |bc -l) )); then
    exit 1
fi

# Lint fluxCalc.py
python -m pylint fluxCalc.py | tee score/result.txt
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' score/result.txt)
echo "Pylint score was $score"
if (( $(echo "$threshold > $score" |bc -l) )); then
    exit 1
fi

# Lint GenerateNoise.py
python -m pylint GenerateNoise.py | tee score/result.txt
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' score/result.txt)
echo "Pylint score was $score"
if (( $(echo "$threshold > $score" |bc -l) )); then
    exit 1
fi

# Lint Generator.py
python -m pylint Generator.py | tee score/result.txt
score=$(sed -n 's/^Your code has been rated at \([-0-9.]*\)\/.*/\1/p' score/result.txt)
echo "Pylint score was $score"
if (( $(echo "$threshold > $score" |bc -l) )); then
    exit 1
fi
