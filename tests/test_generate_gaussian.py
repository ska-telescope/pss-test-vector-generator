import numpy as np
import sys 
sys.path.append('..')
from GenerateGaussian import MakeGaussianProfile

# Test Generate Gaussian
def test_check_inputs_true():
    expected = True
    generator = MakeGaussianProfile(256, 0.05)
    result = generator.check_inputs()
    assert result == expected

def test_check_input_false():
    expected = False
    generator = MakeGaussianProfile(256, 1.2)
    result = generator.check_inputs()
    assert result == expected

def test_profile():
    fixture_profile = np.loadtxt("../fixtures/Gaussian_DC=0.05_BINS=256_FWHM_12.8.asc", unpack=True)
    generator = MakeGaussianProfile(256, 0.05)
    profile = generator.generate_profile()
    for i in range(0, len(profile)):
        assert fixture_profile[i] == profile[i]
    assert len(profile) == 256
