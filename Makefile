#
## Makefile for building and publishing the `test vectors` image.
#
#
## ----------------------------------------------------------------------------
# Variables
# # ----------------------------------------------------------------------------
ARTEFACT_REPO:=artefact.skao.int
MODEL_VIEW_NAME:=ska-pss
IMAGE_NAME:=test-vector-generator
VERSION:=$(shell awk -F= '/.*/{print $$1}' VERSION)

IMAGE:=${ARTEFACT_REPO}/${MODEL_VIEW_NAME}-$(IMAGE_NAME)
#
# ----------------------------------------------------------------------------
#  # Docker helper targets
# ----------------------------------------------------------------------------
#
.PHONY: build
build:  ## Build the docker image
	docker build -t $(IMAGE):$(VERSION) --label CI_COMMIT_SHA=${CI_COMMIT_SHA} --label CI_JOB_ID=${CI_JOB_ID} --label CI_PIPELINE_ID=${CI_PIPELINE_ID} --label CI_PIPELINE_IID=${CI_PIPELINE_IID} --label "CI_COMMIT_AUTHOR=${CI_COMMIT_AUTHOR}" --label CI_COMMIT_REF_NAME=${CI_COMMIT_REF_NAME} --label CI_COMMIT_REF_SLUG=${CI_COMMIT_REF_SLUG} --label CI_COMMIT_SHORT_SHA=${CI_COMMIT_SHORT_SHA} --label CI_COMMIT_TIMESTAMP="${CI_COMMIT_TIMESTAMP}" --label CI_JOB_URL=${CI_JOB_URL} --label CI_PIPELINE_URL=${CI_PIPELINE_URL} --label CI_PROJECT_ID=${CI_PROJECT_ID} --label CI_PROJECT_PATH_SLUG=${CI_PROJECT_PATH_SLUG} --label CI_PROJECT_URL=${CI_PROJECT_URL} --label CI_REPOSITORY_URL=${CI_REPOSITORY_URL} --label CI_RUNNER_ID=${CI_RUNNER_ID} --label CI_RUNNER_REVISION=${CI_RUNNER_REVISION} --label CI_RUNNER_TAGS="${CI_RUNNER_TAGS}" --label "GITLAB_USER_NAME=${GITLAB_USER_NAME}" --label GITLAB_USER_EMAIL=${GITLAB_USER_EMAIL} --label GITLAB_USER_LOGIN=${GITLAB_USER_LOGIN} --label GITLAB_USER_ID=${GITLAB_USER_ID} .

.PHONY: ls
ls:  ## List docker images
	docker image ls --filter=reference="$(IMAGE)"

.PHONY: push
push:   ## Push the image to the remote repository
	docker push "$(IMAGE):$(VERSION)"

